
# Autoscaling prédictif


## Scaling horizontal basé sur des prédictions

Dans cette partie de l'expérimentation, nous allons ajouter une composante "prédictive" à nos HorizontalPodAutoscaler afin d'essayer de dépasser une des limitations de l'HPA : les applicatifs ayant un temps de démarrage long.

En effet, si nous sommes en capacité de prédire que dans 10 minutes nous aurons besoin de n pods supplémentaires, il est devient possible de préempter leur démarrage.

Plusieurs pistes peuvent permettre d'arriver à ce résultat:
  * Fonctionnalités de projection de prometheus


## scaling horizontal : associer prédictif et métriques

Prometheus permet d'effectuer une [projection linéaire](https://prometheus.io/docs/prometheus/latest/querying/functions/#predict_linear) à partir d'une métrique `Gauge`.

Pour pouvoir utiliser cette projection, nous allons définir une rule de prometheus-adapter basée sur le nombre de requêtes par l'ingress-controller, avec une projection à 5 minutes :

```yaml
seriesQuery: '{__name__=~"nginx_ingress_controller_requests",namespace!="",pod!=""}' # 1
resources:
  overrides:  # 2
    exported_namespace:
      resource: namespace
    exported_service:
      resource: service
name:
  matches: ^nginx_ingress_controller_requests   # 3
  as: predicted_requests_per_second_via_ingress
  
metricsQuery: clamp_min(predict_linear(sum(rate(<<.Series>>[5m])) by (<<.GroupBy>>)[5m:], 300), 0) # 4
```

  1. Récupère la métrique `nginx_ingress_controller_requests` pour tout namespace/service.
  1. Associe par les `overrides` la métrique récupérée au service sur lequel les appels sont effectués.
  1. Remplace le nom de la métrique prometheus pour ajouter la notion de prédiction.
  1. Prédiction à 5 minutes, supprimant toute valeur négative via le clamp_min à 0.

### Protocole de test

Pour tester le comportement prédictif, nous avons [créé](predictions/server.py) et [déployé](predictions/dummy-server.yaml) un applicatif se contentant de répondre `OK!` à chaque requête qu'il reçoit. Le but étant de charger l'ingress-controller sans être impacté par le temps de réponse de l'applicatif afin d'isoler au mieux le comportement de la prédiction.

Les tests de charge sont effectués avec [Gatling](https://gatling.io/)

Les scénarii de test sont disponibles dans le dossier [predictions/gatling](predictions/gatling/)

### Test 0 : Référence - HPA utilisant la valeur courante de la métrique

Pour servir de référence, nous avons déployé un [HPA](predictions/hpa-valeur-courante.yaml) utilisant la métrique `requests_per_second_via_ingress` :

```yaml
  - type: Object
    object:
      metric:
        name: requests_per_second_via_ingress
      describedObject:
        apiVersion: v1
        kind: Service
        name: dummyserver
      target:
        type: AverageValue
        averageValue: 10
```

et une montée en charge via [gatling](predictions/gatling/ChargeDummy.java) :

```java
setUp(scn.injectOpen(
    atOnceUsers(1),
    rampUsersPerSec(1).to(30).during(600),
    constantUsersPerSec(30).during(1200),
    rampUsersPerSec(30).to(0).during(600)
  )).protocols(httpProtocol);
```

Nous donnant le résultat suivant :

![test hpa simple](predictions/test-montee-charge-hpa-simple.png)

Il est intéressant de noter que, dans ce cas de figure, le nombre de replicas suit exactement les besoins, mais, dans notre cas d'étude d'une application ayant un temps de démarrage de 5 minutes, ces replicas seront disponibles après le seuil de charge les nécessitant.

### Test 1 : HPA utilisant uniquement les métriques prédictives

Dans ce test, nous avons configuré l'[HPA](predictions/hpa-predictif.yaml) pour utiliser la configuration de métrique prédictive suivante :

```yaml
  - type: Object
    object:
      metric:
        name: predicted_requests_per_second_via_ingress
      describedObject:
        apiVersion: v1
        kind: Service
        name: dummyserver
      target:
        type: AverageValue
        averageValue: 10
```

et une montée en charge via [gatling](predictions/gatling/ChargeDummy.java) :

```java
setUp(scn.injectOpen(
    atOnceUsers(1),
    rampUsersPerSec(1).to(30).during(600),
    constantUsersPerSec(30).during(1200),
    rampUsersPerSec(30).to(0).during(600)
  )).protocols(httpProtocol);
```

Nous donnant le résultat suivant :

![resultat test 1](predictions/test-montee-charge-predictif-seul.png)

A gauche : nombre d'appels/s, à droite: valeur de la métrique prédictive.

Plusieurs point interressants à noter lors de ce premier test :
  1. la valeur maximale vue par la métrique dépasse la valeur effective, ce qui est tout à fait cohérent, mais implique une surréservation de ressources lors des montées en charge.
  1. Au début de la montée en charge, la métrique prédictive est en retard par rapport à la charge réelle, risquant de manquer des pods nécessaires au support de la charge.
  1. Lors de la baisse de charge, la métrique prédictive est d'abord en retard puis chute bien plus rapidement que la valeur réelle, entrainant une suppression de pods qui seraient pourtant nécessaires.

**L'utilisation de la prédiction seule ne permet donc pas de gérer la scaling de notre application**

### Test 2 : HPA utilisant les métriques prédictives et la valeur courante

Lorsque plusieures métriques sont configurées dans un HPA, la métrique produisant la valeur de nombre de réplicas la plus élevée est celle utilisée pour définir le nombre de réplicas.

Ce comportement devrait permettre de traiter les problématiques des points 2 et 3 du test précédent, le but recherché étant de toujours disposer de suffisamment de pods dans un premier temps tout en anticipant la charge potentielle à venir.

La configuration de l'[HPA](predictions/hpa-predictif-et-valeur-courante.yaml) devient :

```yaml
  - type: Object
    object:
      metric:
        name: predicted_requests_per_second_via_ingress
      describedObject:
        apiVersion: v1
        kind: Service
        name: dummyserver
      target:
        type: AverageValue
        averageValue: 10
  - type: Object
    object:
      metric:
        name: requests_per_second_via_ingress
      describedObject:
        apiVersion: v1
        kind: Service
        name: dummyserver
      target:
        type: AverageValue
        averageValue: 10
```

Le test de montée en charge via [gatling](predictions/gatling/ChargeDummy.java) sera identique au test précédent.

Le résultat obtenu est le suivant :

![test hpa avec prediction et courant](predictions/test-montee-charge-predictif-courant-40min.png)

Nous pouvons constater que le point 3 du test précédent (suppression de replicas trop tôt) est bien traité par l'ajout de la métrique courante, l'HPA utilisant la valeur la plus élevée.

### Test 3 : HPA utilisant les métriques prédictives et la valeur courante / montée en charge plus lente

Dans ce dernier test, nous avons utilisé le même [HPA](predictions/hpa-predictif-et-valeur-courante.yaml) que précédemment, et augmenté le temps de montée et descente en charge via gatling :

```java
setUp(scn.injectOpen(
  atOnceUsers(1),
  rampUsersPerSec(1).to(30).during(1800),
  constantUsersPerSec(30).during(1200),
  rampUsersPerSec(30).to(0).during(1200)
  )).protocols(httpProtocol);
```

Donnant le résultat suivant:

![test montee charge longue](predictions/test-montee-charge-longue.png)

Ce test est plus représentatif d'une montée en charge "normale" et l'on peut constater que le point 2 du test 1 (retard de la prédiction) est bien traité par l'ajout de la métrique courante, ainsi que par la prédiction, qui, dans ce scénario, prédit bien le besoin de nouveaux réplicas.

Il est également intéressant de constater que, la pente de montée en charge étant plus douce, la prédiction dépasse moins la valeur réelle que lors du premier test. Ce dépassement est effectivement une fonction de la pente ainsi que de la distance de projection (5 minutes dans nos exemples). Un exemple probant se trouve dans [predictions/test-montee-charge-predictif-courant-20min.png](predictions/test-montee-charge-predictif-courant-20min.png).
