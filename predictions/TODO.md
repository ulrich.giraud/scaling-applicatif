## IA prédiction
P.Galtier
Nicolas Calabrese



docker run -p 8080:8080 --rm docker-dev-virtual.repository.pole-emploi.intra/labo/dummy-server:0.0.1
predict_linear(nginx_ingress_controller_requests{exported_namespace="supervision"}[5m], 30)
predict_linear(apiserver_longrunning_requests{component="apiserver", endpoint="https", group="apiextensions.k8s.io", instance="10.209.16.15:6443", job="apiserver", namespace="default", resource="customresourcedefinitions", scope="cluster", service="kubernetes", verb="WATCH", version="v1"}[5m], 300)
predict_linear(ingress_requests:by_namespace_service:sum[5m],30)

sum(nginx_ingress_controller_requests{exported_namespace="supervision"})

predict_linear(sum(nginx_ingress_controller_requests) by (exported_namespace, exported_service)[5m:], 60)

curl -k --proxy http://localhost:8080 -vL https://dummyserver.testinfra.k8s.pole-emploi.intra
clamp_min(predict_linear(sum(rate(nginx_ingress_controller_requests[5m])) by (exported_namespace, exported_service)[5m:], 300), 0)
sum(rate(nginx_ingress_controller_requests[1m])) by (exported_namespace, exported_service)