
import java.time.Duration;
import java.util.*;

import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;
import io.gatling.javaapi.jdbc.*;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;
import static io.gatling.javaapi.jdbc.JdbcDsl.*;

public class ChargeDummyFast extends Simulation {

  {
    HttpProtocolBuilder httpProtocol = http
      .baseUrl("https://dummyserver")
      .inferHtmlResources()
      .acceptHeader("*/*")
      .userAgentHeader("curl/7.85.0");
    


    ScenarioBuilder scn = scenario("ChargeDummyFast")
      .exec(
        http("request_0")
          .get("/")
      );

	  setUp(scn.injectOpen(
      atOnceUsers(1),
      rampUsersPerSec(1).to(30).during(300),
      constantUsersPerSec(30).during(600),
      rampUsersPerSec(30).to(0).during(300)
      )).protocols(httpProtocol);
  }
}
