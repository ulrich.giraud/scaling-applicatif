# scaling-applicatif


## Objectifs de l'auto-scaling applicatif

Nous déployons actuellement nos applicatifs sur des clusters kubernetes dont les ressources sont finies. Chacune de nos applications réserve une partie des ressources partagées du cluster afin de pouvoir fonctionner. 

Une application doit fonctionner en mode nominal, de croisière mais également pouvoir supporter des pic de charge sans impact utilisateur.

Actuellement, nous sur-allouons des ressources ( par les requests et par le nombre de réplicas ) à ces applications afin d'anticiper ces charges de fonctionnement.
Or, les pics de charge d'une application ne se font pas forcément au même moment qu'une autre.

L'utilisation des `requests` ne devrait pas tenir compte des pics de charge d'un applicatif, mais uniquement définir la consommation en régime de croisière, afin de maximiser leur utilisation.

L'auto scaling applicatif devrait permettre dans un premier temps d'optimiser le partage des ressources des clusters en réagissant à la consommation réelle d'un applicatif.

L'auto scaling ne peut s'envisager que de manière bi-directionnelle, l'augmentation seule s'apparenterait sinon à une fuite de ressources.

Une fois l'auto-scaling applicatif utilisé à l'échelle, il deviendra possible d'atteindre le deuxième niveau : l'auto-scaling de clusters kubernetes.

## Limites d'utilisation de l'auto-scaling

Dans les cas suivants :

  * Charge linéaire de l'applicatif :
    L'auto-scaling dans ce cas serait inutile, cependant n'impacterait en rien l'applicatif.

  * Des ressources minimales permettent de répondre à toutes les situations de l'applicatif:
    Idem

  * Temps de démarrage d'un nouveau réplica long ou pics de charge très courts:
    Si le temps de démarrage d'un nouveau réplica est plus long que la durée d'un pic de charge, alors il sera impossible pour le système de réagir a temps, rendant ainsi l'autoscaling inefficace et contre-productif.


## HorizontalPodAutoscaler

### Utilisation des métriques standards de l'auto-scaling

Kubernetes dispose d'un objet [HorizontalPodAutoscaler](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/) qui permet d'appliquer a un objet scalable ( StatefulSet / Deployment ) des règles de scalabilité horizontale.

Par défaut, seules deux métriques sont utilisables : utilisation CPU et Mémoire par rapport aux `requests` positionnées. Un exemple d'utilisation est présent dans le fichier [hpa/hpa-standard.yaml](hpa/hpa-standard.yaml) de ce projet, ainsi qu'un deployment correspondant.

Ce mode de fonctionnement permet de réagir à la charge de l'applicatif sans utiliser de système externe, et gère le scale up et down.


### Utilisation de métriques custom pour l'auto-scaling

Il est possible de créer des régles personnalisées que l'on peut utiliser avec les objets [HorizontalPodAutoscaler](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/). Pour ce faire, il faut utiliser un `adapter` permettant de fournir les métriques désirées à Kubernetes Metrics API.

Dans notre cas exemple, nous nous sommes appuyés sur [prometheus-adapter](https://github.com/kubernetes-sigs/prometheus-adapter), ce qui devrait convenir à l'ensemble de nos clusters.

__déploiement de l'adapter:__

```bash
helm upgrade --install --namespace supervision --create-namespace --values values-adapter.yaml --version 4.2.0 prometheus-adapter prometheus-adapter
```

__règles prometheus:__

L'adapter permettant de transformer des métriques prometheus en métriques au format de l'API kubernetes, il est nécessaire de lui fournir des règles de conversion.
Notre exemple s'appuie sur le nombre de requêtes par secondes via l'ingress controller, la règle utilisée est la suivante:

```yaml
seriesQuery: '{__name__=~"nginx_ingress_controller_requests",namespace!="",pod!=""}'
resources:
  overrides:
    exported_namespace:   # Label de la métrique
      resource: namespace # dont la valeur sera utilisée pour le namespace
    exported_service:     # Label de la métrique
      resource: service   # dont la valeur sera utilisée pour le nom de service
name:
  matches: ^nginx_ingress_controller_(.*)
  as: ${1}_per_second_via_ingress

metricsQuery: sum(rate(<<.Series>>{<<.LabelMatchers>>}[2m])) by (<<.GroupBy>>)
```

L'ingress controller effectuant des appels sur les services et non directement sur les pods, les `overrides` permettent de mapper cette donnée à partir des labels de la métrique prometheus.

__Utilisation dans l'HPA:__

Le fichier [hpa/hpa-metriques-prom.yaml](hpa/hpa-metriques-prom.yaml) contient une définition d'HPA effectuant un scaling automatique à partir du nombre d'appels par seconde via l'ingress et démontre l'utilisation d'une métrique attachée à un objet tiers.

__Nota Bene:__ Les règles de l'adapter sont appliquées pour un cluster entier, il est possible via la query de limiter à un namespace, mais il n'existe actuellement pas de CRD/operateur pour ajouter des règles dynamiquement.

### Note sur l'affinité de session

L'auto scaling peut être compatible avec l'affinité de session, le loadbalancer doit pouvoir allouer les nouvelles sessions sur les nouveaux réplicas, et lors de l'arrêt, un réplica ne doit rendre la main qu'une fois l'ensemble des sessions réellement terminées.


### Exemple avec concourse

Les workers concourse sont un parfait exemple de besoin de métriques custom. En effet, chaque job exécuté peut charger à 100% la CPU d'un worker, ce qui est tout à fait normal pour un outil de CI. Cependant, 100% d'utilisation CPU n'indique pas, seul, une surcharge du worker.
Du coté mémoire, le simple fait d'avoir une utilisation mémoire faible n'indique pas non plus que le worker n'est pas surchargé.

Il est donc nécessaire de s'appuyer sur d'autres métriques, fournies par les workers. Nous avons sélectionné les deux métriques suivantes:
  * [concourse_workers_containers](https://concourse-ci.org/metrics.html): nombre de containers en execution sur le worker
  * [concourse_workers_volumes](https://concourse-ci.org/metrics.html): nombre de volumes sur le worker


Pour disposer des métriques du worker la rule prometheus-adapter est :

```yaml
- seriesQuery: '{__name__=~"concourse_workers_.*",namespace!="",pod!=""}'
  resources:
    overrides:
      namespace:
        resource: namespace
      worker:
        resource: pod  # Mapping entre le nom du worker et le pod l'exécutant
  name:
    matches: ^(.*)
    
  metricsQuery: <<.Series>>{<<.LabelMatchers>>}
```

Pour générer un HPA s'appuyant sur ces métriques, le chart helm de concourse permet la définition de l'HPA:

```yaml
concourse:
  worker:
    autoscaling:
      maxReplicas: 10
      minReplicas: 2
      behavior:
        scaleDown:
          stabilizationWindowSeconds: 1200
          selectPolicy: Min
          policies:
            - type: Percent
              value: 10
              periodSeconds: 300
        scaleUp:
          stabilizationWindowSeconds: 60
      builtInMetrics:
        - type: Resource
          resource:
            name: cpu
            target:
              type: Utilization
              averageUtilization: 80
      customMetrics:
        - type: Pods
          pods:
            metric:
              name: concourse_workers_containers
            target:
              type: AverageValue
              averageValue: 150
        - type: Pods
          pods:
            metric:
              name: concourse_workers_volumes
            target:
              type: AverageValue
              averageValue: 450
```

Cette configuration a été appliquée sur un concourse, et était adaptée à l'utilisation de ce concourse. Avec la `rule` définie dans prometheus-adapter, il est possible de se baser sur toute métrique du worker, notamment `concourse_worker_tasks` qui pourrait permettre de scaler par nombre de taches en cours d'éxecution.

__Nota bene:__ L'utilisation d'auto-scaling sur concourse nécessite des jobs de courte durée.
Il est possible de configurer la `stabilizationWindowSeconds` du `scaleDown` pour mitiger cette problématique, cependant, cela ne garantit pas le bon fonctionnement sur les jobs longs, et est à utiliser avec parcimonie du au fait que l'HPA sera moins réactif et donc augmentera le gaspillage de ressources.

### Calcul du nombre de réplicas

La règle de calcul du nombre de réplicas est la suivante : `desired_replicas = current_replicas * (current_metric_value / desired_metric_value)`.

Le type de calcul est spécifiable dans chaque métrique utilisée par l'HPA et présente 3 valeurs:
  * Value : Augmente le nombre de réplicas dès que la métrique dépasse la valeur spécifiée, sans utiliser le nombre de réplicas actuels
  * AverageValue : Divise la valeur par le nombre de replicas actuels et augmente le nombre de réplicas si cette valeur dépasse la cible.
  * Utilization : Augmente le nombre de réplicas lorsque le pourcentage d'utilisation de la métrique dépasse le pourcentage spécifié. Exemple : 80% d'utilisation CPU. Uniquement disponible pour les métriques de type Resource ( aka CPU et mémoire )

Lorsque plusieures métriques sont spécifiées dans un HPA, la métrique remontant le plus grand nombre de réplicas cible sera utilisée en toutes circonstances.

### Requêtes diverses

```bash
kubectl get --raw /apis/metrics.k8s.io/v1beta1/namespaces/test-hpa/pods/php-apache-8bb6bbbb4-ct8c8 | jq
kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1 | jq
kubectl get --raw "/apis/custom.metrics.k8s.io/v1beta1/namespaces/test-hpa/services/*/requests_per_second_via_ingress" | jq
kubectl get --raw "/apis/custom.metrics.k8s.io/v1beta1/namespaces/prediction/services/*/requests_duration_via_ingress_traefik" | jq
kubectl get --raw "/apis/custom.metrics.k8s.io/v1beta1/namespaces/prediction/services/*/requests_per_second_via_ingress_traefik" | jq
```

## VerticalPodAutoscaler

Le `VerticalPodAutoscaler` n'est pas un composant présent par défaut dans Kubernetes, il est nécessaire de l'installer :

```bash
helm repo add cowboysysop https://cowboysysop.github.io/charts/
helm upgrade --install --namespace vpa --create-namespace --values values-vpa.yaml vpa cowboysysop/vertical-pod-autoscaler
```

### Cas d'exemple

Pour tester le scaling vertical, nous avons déployé les composants suivants :

  * [vpa/consomemoire.yaml](vpa/consomemoire.yaml) : Ce composant, lorsque appelé en GET avec le paramètre m=taille_memoire_en_mo consomme la taille spécifiée en mémoire supplementaire. Il permettra de tester le scaling sur la consommation mémoire.
  * [vpa/php-apache.yaml](vpa/php-apache.yaml) : simple serveur apache, déployé avec un générateur de charge pour simuler un composant ayant des pics de charge.
  * [vpa/hamster.yaml](vpa/hamster.yaml) : composant consommant 1/2 CPU par pod pour simuler une charge lissée.

### Fonctionnement de l'autoscaler vertical

L'autoscaler vertical s'appuie sur les métriques CPU et mémoire remontées par le metrics-server. Sachant qu'il permet d'ajuster les requests et limites, ces données sont suffisantes à son action.

Il génère des recommandations, via le composant `recommender`, à partir de l'historique de ces données et dispose de 4 modes de fonctionnement concernant la mise a jour :
  * Auto / Recreate : ces deux modes sont similaires, l'autoscaler effectuera le re-démarrage du/des pods si la recommandation générée est suffisamment éloignée des requests/limits positionnées actuellement sur le pod
  * Initial : Les recommandations ne sont appliquées aux pods qu'à leur création, l'autoscaler ne re-démarre pas les pods
  * Off : Aucune action sur les pods, les recommandations sont simplement consultables dans le status de l'objet VerticalPodAutoscaler

L'application des recommandations ne concerne que les objets `Pod`, aucune modification n'est faite sur les deployments/statefulset/replicasets. Pour y parvenir, un MutatingWebhook appelant le composant `admission controller`, écoute les créations de Pod et modifie à la volée les valeurs de ressources.

L'éviction automatique des pods est gérée par le composant `updater`, et s'effectue en fonction du mode de fonctionnement configuré, et de la différence entre recommandation et valeur actuelle (Voir ci-dessous).

### Focus sur le recommender

Le composant recommender produit 3 recommandations :

  * lowerBound : Utilisé dans le calcul de l'éviction des pods pour diminuer les ressources allouées
  * upperBound : Utilisé dans le calcul de l'éviction des pods pour augmenter les ressources allouées
  * target : Valeurs appliquées a la création des pods

Pour produire ces recommandations, les valeurs de consommation CPU et mémoire des pods sont stockées dans un objet VerticalPodAutoscalerCheckpoint sous la forme d'histogramme.
La valeur de target est produite a partir de la valeur de l'histogramme, multiplié par une pourcentage (paramètre `target-cpu-percentile`), suivi d'un ajout d'une marge par rapport a l'utilisation courante (paramètre `recommendation-margin-fraction`.
)

Les valeurs upperBound et lowerBound sont évolutives en fonction de la profondeur d'historique de consommation de ressources du pod ( nommé confidence factor ) : `scaledResource = originalResource * (1 + 1/confidence)^exponent.`

__Extrait du code de l'estimator:__
```go
	// Using the confidence multiplier 1 with exponent +1 means that
	// the upper bound is multiplied by (1 + 1/history-length-in-days).
	// See estimator.go to see how the history length and the confidence
	// multiplier are determined. The formula yields the following multipliers:
	// No history     : *INF  (do not force pod eviction)
	// 12h history    : *3    (force pod eviction if the request is > 3 * upper bound)
	// 24h history    : *2
	// 1 week history : *1.14
	upperBoundEstimator = WithConfidenceMultiplier(1.0, 1.0, upperBoundEstimator)

	// Using the confidence multiplier 0.001 with exponent -2 means that
	// the lower bound is multiplied by the factor (1 + 0.001/history-length-in-days)^-2
	// (which is very rapidly converging to 1.0).
	// The formula yields the following multipliers:
	// No history   : *0   (do not force pod eviction)
	// 5m history   : *0.6 (force pod eviction if the request is < 0.6 * lower bound)
	// 30m history  : *0.9
	// 60m history  : *0.95
	lowerBoundEstimator = WithConfidenceMultiplier(0.001, -2.0, lowerBoundEstimator)
```

Pour mieux se représenter cette convergence, voici [un graphique desmos](https://www.desmos.com/calculator/awy4kp4uq3?lang=fr) représentant les formules ci dessus pour une target de 100

Cette méthode de calcul de bornes et d'éviction rend les VPA peu réactifs, et ce comportement est souhaitable afin d'éviter de ré-demarrer les pods souvent ou pour de petits ajustements.

Cette réactivité est encore impactée par la gestion de l'histogramme, ce dernier, par défaut, a une demi-vie de 24H. Ce paramètre est ajustable via `cpuHistogramDecayHalfLife`. Pour les besoins de tests nous l'avons passé a "5m", augmentant ainsi énormément la réactivité des VPAs.

__Gestion des OOMKill:__

Les recommandations mémoire faites par le VPA disposent également d'un ajustement sur la détection d'un OOMKill, dans ce cas, les bornes sont augmentées immédiatement de 20% ( contrôlable par le paramètre `oom-bump-up-ratio`).



## Conclusion

L'autoscaling d'applications est LA prochaine étape pour gérer efficacement les ressources partagées de nos clusters.

Nous avons deux possibilités qui permettent de gérer des cas d'utilisations très différents avec les HPA et les VPA.

### HorizontalPodAutoscaler

Les HPA disposent d'une grande réactivité sur les montées en charge, et sont très souples dans leur configuration. Cependant, les métriques disponibles par défaut (utilisation CPU et mémoire) ne sont pas adaptées a tous les types de workload, et pour atteindre leur plein potentiel, l'utilisation du prometheus-adapter est indispensable.

Leur seule limite d'utilisation concerne les workload n'ayant pas la capacité à se scaler horizontalement, par exemple, un cluster raft n'aurait aucun intérêt à être géré par un HPA. Il en est de même pour une base de données type postgresql simple.

### VerticalPodAutoscaler

Les VPA sont encore une fonctionnalité en béta (et ils le sont depuis longtemps) non intégrée par défaut dans les clusters Kubernetes.

Ils permettent d'ajuster les réservations et limites de ressources via leurs recommandations, et potentiellement de les appliquer automatiquement sur les pods.
Contrairement aux HPA, la réactivité est extrèmement faible (sauf cas d'OOMKill), et ce comportement permet d'affiner la recommendation ainsi qu'éviter des redémarrage intempestifs et fréquents de pods. Leur but n'est donc clairement pas de réagir à des pics de charge, mais plutôt d'obtenir des recommandations et des pods stabilisés dans le temps.

Il est possible dans tous les cas de les utiliseren mode non automatique (`Off`), qui permettra, sur le long terme, d'obtenir des recommandations sur les ressources à allouer à une instance d'application.

Le mode d'utilisation (`Auto`) serait tout indiqué dans les cas où les HPA ne seraient pas efficace: les workload n'ayant pas la capacité à se scaler horizontalement.


### HorizontalPodAutoscaler + VerticalPodAutoscaler

Pourquoi ne pas réagir aux pics de charge avec un HPA et affiner sur le long terme les resources avec un VPA?
C'est faisable, à condition de ne pas utiliser les métriques CPU et mémoire dans l'HPA, pour des raisons évidentes...

Cependant, des effets de bord imprédictibles peuvent se présenter, le fait de scaler horizontalement aura forcément un impact sur les utilisations CPU et mémoire, rendant le calcul des recommandations moins précis, voir totalement éronné dans le cas ou les pics de charge sont très fréquents avec un fort impact sur la conso CPU et/ou mémoire.

Maitriser les HPA et les VPA, ainsi que leur impact sur l'applicatif, seront un prérequis avant d'étudier la conjugaison des deux.


## Ressources

* https://github.com/kubernetes/autoscaler
* https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler
* https://artifacthub.io/packages/helm/cluster-autoscaler/cluster-autoscaler
* https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
* https://www.cncf.io/blog/2023/02/24/optimizing-kubernetes-vertical-pod-autoscaler-responsiveness/
* https://medium.com/@kewynakshlley/performance-evaluation-of-the-autoscaling-strategies-vertical-and-horizontal-using-kubernetes-42d9a1663e6b

## Pour aller plus loin

* [Prédire la charge pour scaler horizontalement](scaling-predictif.md)